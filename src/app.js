const express = require('express');
const path = require('path');
const hbs = require('hbs');
const geocode = require('./utils/geocode');
const forecast = require('./utils/forecast');

const app = express();
const port = process.env.PORT || 3000;

/** Paths for Express config */
const publicPath = path.join(__dirname, '../public');
const viewsPath = path.join(__dirname, '../templates/views');
const partialsPath = path.join(__dirname, '../templates/partials');

/** Setup handlebars engine and views location */
app.set('view engine', 'hbs');
app.set('views', viewsPath);
hbs.registerPartials(partialsPath);

/** Setup static directory to serve */
app.use(express.static(publicPath));

app.get('', (req, res) => {
  res.render('index', {
    title: 'Weather App',
    name: 'RogFed'
  });
})

app.get('/about', (req, res) => {
  res.render('about', {
    title: 'About Me',
    name: 'RogFed'
  });
})

app.get('/help', (req, res) => {
  res.render('help', {
    title: 'Help',
    helpText: 'This is some helpful text.',
    name: 'RogFed'
  });
})

app.get('/weather', (req, res) => {
  const {address} = req.query;
  if(!address){
    return res.send({
      error: 'You must provide an address.'
    });
  }

  geocode(address, (error, {latitude, longitude, location, shortcode} = {}) => {
    if(error){
      return res.send({error});
    }

    forecast(latitude, longitude, (error, data) => {
      if(error){
        return res.send(error);
      }

      return res.send({location, forecast: data, address, shortcode});
    });
  });
});

app.get('/products', (req, res) => {
  if(!req.query.city){
    return res.send({
      error: 'You must provide a search term.'
    })
  }
  
  console.log(req.query);
  res.send({
    products: {}
  })
})

app.get('/help/*', (req, res) => {
  res.render('404', {
    title: '404',
    name: 'RogFed',
    message: 'Help article not found.'
  })
})

app.get('*', (req, res) => {
  res.render('404', {
    title: '404',
    name: 'RogFed',
    message: 'Page not found.'
  })
})

app.listen(port, () => {
  console.log(`Server is up on port ${port}.`);
});