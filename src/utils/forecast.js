const request = require('request');

const forecast = (latitude, longitude, callback) => {
  const url = `https://api.darksky.net/forecast/00b65a8eeb27d45a00c377b90106e4e6/${latitude},${longitude}?units=si`;

  request({url, json: true}, (error, {body}) => {
    if(error){
      callback('Unable to connect to weather service.', undefined);
    } else if(body.error){
      callback('unable to find location.', undefined);
    } else {
      const temp = body.currently.temperature;
      const chanceOfRain = body.currently.precipProbability;
      const summary = body.daily.data[0].summary;
      callback(undefined, `${summary} It is currently ${temp} degrees out. There is a ${chanceOfRain}% of rain.`)
    }
  })
}

module.exports = forecast;