const form = document.querySelector('form');
const search = document.querySelector('input');
const messageTwo = document.querySelector('#message-2');
const messageOne = document.querySelector('#message-1');
messageOne.textContent = 'Loading...';
messageTwo.textContent = '';

form.addEventListener('submit', e => {
  e.preventDefault();
  const location = search.value;
  
  fetch(`/weather?address=${encodeURIComponent(location)}`).then(response => {
    response.json().then(data => {
      if(data.error){
        messageOne.textContent = data.error;
        messageTwo.textContent = '';
      } else {
        messageOne.textContent = `${data.location}, ${data.shortcode}`;
        messageTwo.textContent = data.forecast;
      }
    });
  });
})